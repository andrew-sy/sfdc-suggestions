import { LightningElement, api } from 'lwc';
import { ACCOUNT_TYPE, ACTION_TYPE } from './enums';

export default class SuggestionCard extends LightningElement {

  @api
  accountName = 'Account Name';
  @api
  accountType = ACCOUNT_TYPE.HCP;
  @api
  suggestionTitle = 'Suggestion Title';
  @api
  suggestionReason = 'suggestion reason text - This is an email suggestion.他部門​該当なし​該当なし​不明​該当なし​不明送信ターゲットなし Product:Dummy Is Person Account or not';

  actionType = ACTION_TYPE.NONE;

  get iconName() {
    return this.accountType === ACCOUNT_TYPE.HCP ? 'standard:person_account' : 'standard:account';
  }

  get isActioned() {
    return this.actionType !== ACTION_TYPE.NONE;
  }

  get isDismissed() {
    return this.actionType === ACTION_TYPE.DISMISS;
  }

  handleDismissClick() {
    this.actionType = ACTION_TYPE.DISMISS;
  }

  handleCompleteClick() {
    this.actionType = ACTION_TYPE.COMPLETE;
  }

}