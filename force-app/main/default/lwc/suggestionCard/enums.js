export const ACCOUNT_TYPE = {
    HCP: 'hcp',
    HCO: 'hco'
  }
  
  export const ACTION_TYPE = {
    NONE: 0,
    DISMISS: 1,
    COMPLETE: 2
  }