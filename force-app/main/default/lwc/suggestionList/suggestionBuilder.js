const SUGGESTED = 'SUGGESTED';

const toSuggestions = (suggestions) => {
	return (suggestions || []).map((suggestion) => {
		const {
			accountUID,
			accountName,
			facilityName,
			specialty,
			accountTypeUID,
			actionTypeUID,
			channelUID,
			details,
			id,
			isCritical,
			isDismissed,
			priorityScore,
			reasons,
			runStartDateLocal,
			suggestedDate,
			latitude,
			longitude,
		} = suggestion;
		const messages = (details || [])
			.map(({ messageNames }) => messageNames)
			.reduce((accumulator, value) => accumulator.concat(value), []);
		const products = (details || []).map(({ productName }) => productName);
		const suggestedReason = (reasons || []).find(
			({ crmFieldName }) => crmFieldName === SUGGESTED
		);
		const title = suggestedReason ? suggestedReason.text.trim() : '';
		const reasonText = (reasons || [])
			.filter(({ crmFieldName }) => crmFieldName !== SUGGESTED)
			.sort((a, b) => a.rank - b.rank)
			.map(({ text }) => `<li>${text}</li>`)
			.join(' ');

		return {
			account: {
				accountId: accountUID,
				accountName,
				accountType: accountTypeUID,
				facilityName,
				specialty,
			},
			suggestion: {
				id,
				accountId: accountUID,
				externalId: id,
				priorityScore: `${priorityScore}`,
				isCritical,
				isDismissed,
				reasonText: `<ul>${reasonText}</ul>`,
				runDate: runStartDateLocal,
				suggestedDate,
				postDate: suggestedDate, // TODO: Need confirm this field
				actionTaken: isDismissed ? 'Suggestions Dismissed' : 'No Action Taken', // Default value
				channel: channelUID,
				actionChannel: actionTypeUID,
				products,
				messages,
				title,
				latitude,
				longitude,
			},
		};
	});
};

export const buildSuggestionData = (data) => {
    const suggestions = toSuggestions(data);

    const criticalSugs = [];
    const normalSugs = [];

    suggestions.forEach((el) => {
        const sug = {};
        sug.id = el.suggestion.id;
        sug.accountId = el.account.accountId;
        sug.accountType = el.account.accountType;
        sug.accountName = el.account.accountName;
        sug.facilityName = el.account.facilityName;
        sug.specialty = el.account.specialty;
        sug.isCritical = el.suggestion.isCritical;
        sug.isDismissed = el.suggestion.isDismissed;
        sug.reasonText = el.suggestion.reasonText;
        sug.title = el.suggestion.title;
        sug.channel = el.suggestion.channel;
        sug.products = el.suggestion.products;
        sug.priorityScore = el.suggestion.priorityScore;

        if (!sug.isDismissed) {
            if (sug.isCritical) {
                criticalSugs.push(sug);
            } else {
                normalSugs.push(sug);
            }
        }
    });
    criticalSugs.sort((a, b) => {
        return a.priorityScore < b.priorityScore ? 1 : -1;
    });
    normalSugs.sort((a, b) => {
        return a.priorityScore < b.priorityScore ? 1 : -1;
    });
    return criticalSugs.concat(normalSugs);
};