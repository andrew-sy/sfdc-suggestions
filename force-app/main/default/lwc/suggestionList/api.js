const RBAC_URL = "https://aimqa-api.aktana.com/rbac-api/v1";

export const getApplications = (authorization) => {
    return fetch(`${RBAC_URL}/profile/applications`, {
        method: 'GET',
        headers: {
            'content-type': 'application/json',
            'Authorization': authorization
        },
        mode: 'cors'
    }).then(response => {
        return response.json();
    })
}

export const getApplicationInfo = (appId, authorization) => {
    return fetch(`${RBAC_URL}/profile/applications/${appId}`, {
        method: 'GET',
        headers: {
            'content-type': 'application/json',
            'Authorization': authorization
        },
        mode: 'cors'
    }).then(response => {
        return response.json();
    })
}

export const getTokenByCompany = (companyCode, companyToken, userId) => {
    return fetch(`${RBAC_URL}/authentication/client`, {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        mode:'cors',
        body: JSON.stringify({
            "client_id": "sttv_lwc",
            "company_code": companyCode,
            "company_token": companyToken,
            "external_id": userId
        })
    }).then(response => {
        return response.json();
    });
}

export const getTokenByApplication = (appId, appToken, userId) => {
    return fetch(`${RBAC_URL}/authentication/application`, {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        mode:'cors',
        body: JSON.stringify({
            "application_id": appId,
            "application_token": appToken,
            "client_id": "sttv_lwc",
            "user_external_id": userId
        })
    }).then(response => {
        return response.json();
    });
}

export const getSuggestions = (dseApi, authorization, actorUID) => {
    const url = `${dseApi}/api/v3.0/Results/suggestions/${actorUID}`;
    return fetch(url, {
        method: 'GET',
        headers: {
            'content-type': 'application/json',
            'Authorization': authorization
        },
        mode: 'cors'
    }).then(response => {
        return response.json();
    });
}