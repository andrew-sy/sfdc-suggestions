import { LightningElement, wire } from 'lwc';
import { getApplicationInfo, getSuggestions, getTokenByApplication } from './api';
import { buildSuggestionData } from './suggestionBuilder';
import Id from '@salesforce/user/Id';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import EMAIL_FIELD from '@salesforce/schema/User.Email';
import findAppTokenByAppType from '@salesforce/apex/AppTokenAKTController.findAppTokenByAppType';

const APPLICATION_TYPE = 'APPLICATION_STTV';

export default class SuggestionList extends LightningElement {
    actorUID = Id;
    loading = false;
    suggestions = [];

    @wire(getRecord, {recordId: Id, fields: [EMAIL_FIELD]})
    userInfo;

    get userEmail() {
        return getFieldValue(this.userInfo.data, EMAIL_FIELD);
    }

    connectedCallback() {
        this.refresh();
    }

    get hasSuggestion() {
        return this.suggestions.length > 0;
    }

    async refresh() {
        try {
            this.loading = true;
            await this.fetchData();
        } catch (e) {
            console.error('fetch suggestions error:' + e);
        } finally {
            this.loading = false;
        }
    }

    async fetchData() {
        const aktToken = await findAppTokenByAppType({appType: APPLICATION_TYPE});
        if (!aktToken) {
            return;
        }
        const appId = aktToken.appId_akt__c;
        const appToken = aktToken.token_akt__c;
         // const userId = this.actorUID;
        const userId = '00580000002EZs9AAG'; // fixed the user id since salesforce environment not align with DSE

        // const appId = 'f0adb841-c105-4625-bbb6-44b85f11dff9';
        // const appToken = 'SuxEJ9gceWuP5PKIDyuvO0u1R8h1Lg_vnbbof19AHZyv6-kzUOZr64YWN0l2UaNKmqA52canMs05hboV1l2OiYKvQPUYTQ4d6AXu_v5N9BxCdvbRRX8CSz6PoNY25WNmcYw1h6pGa_rtbZ8Jqv-ht7Az4oiTEhEkp9VFz62_h50';

        const tokenObj = await getTokenByApplication(appId, appToken, userId);
        const authCode = `Bearer ${tokenObj.access_token}`;
        const sttvApp = await getApplicationInfo(appId, authCode);

        if(sttvApp) {
            const dseApi = sttvApp.configuration.apis.APPLICATION_DSE.url;
            const data = await getSuggestions(dseApi, authCode, userId);
            this.suggestions = buildSuggestionData(data);
        }
    }

}