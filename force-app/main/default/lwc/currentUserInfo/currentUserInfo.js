import { LightningElement, wire } from 'lwc';
import Id from '@salesforce/user/Id';
import { getRecord } from 'lightning/uiRecordApi';
import NAME_FIELD from '@salesforce/schema/User.Name';
import EMAIL_FIELD from '@salesforce/schema/User.Email';

export default class CurrentUserInfo extends LightningElement {
    id = Id;
    name;
    email;
    error;

    @wire(getRecord, {recordId: Id, fields: [NAME_FIELD, EMAIL_FIELD]})
    userInfo({data, error}) {
        if (data) {
            this.name = data.fields.Name.value;
            this.email = data.fields.Email.value;
        } else if (error) {
            this.error = error;
        }
    }
}