import { LightningElement } from 'lwc';
import getUserInfoDetailByMyId from '@salesforce/apex/UserInfoDetailController.getUserInfoDetailByMyId';

export default class UserInfoDetail extends LightningElement {
    lastName;
    firstName;
    address;
    myId = 'ie7wk2ds';

    connectedCallback() {
        this.fetchData();
    }

    async fetchData() {
        const userInfo = await getUserInfoDetailByMyId({myId: this.myId});
        if (userInfo) {
            this.lastName = userInfo.lastName__c;
            this.firstName = userInfo.firstName__c;
            this.address = userInfo.address__c;
        }

    }
}