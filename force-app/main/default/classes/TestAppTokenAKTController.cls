@isTest
public with sharing class TestAppTokenAKTController {
    public static void createAppToken() {
        AppToken_akt__c appToken = new AppToken_akt__c(
            appName_akt__c = 'test_appName',
            appId_akt__c = 'test_appId',
            appType_akt__c = 'APPLICATION_STTV',
            token_akt__c = 'test_token'
        );

        insert appToken;
    }

    @isTest
    static void findAppTokenByAppType() {
        TestAppTokenAKTController.createAppToken();

        Test.startTest();
        AppToken_akt__c appToken = AppTokenAKTController.findAppTokenByAppType('APPLICATION_STTV');
        Test.stopTest();

        System.assertEquals('test_appId', appToken.appId_akt__c, 'the appId_akt__c is not match');
        System.assertEquals('test_token', appToken.token_akt__c, 'the token_akt__c is not match');
    }
}