@isTest
public class TestUserInfoDetailController {
    public static void createUserInfo() {
        UserInfo__c userInfo = new UserInfo__c(
            address__c = 'test_address',
            firstName__c = 'test_first',
            lastName__c = 'test_last',
            myId__c = 'test_myId'
        );

        insert userInfo;
    }
    
    @isTest
    static void getUserInfoDetailByMyId(){
        TestUserInfoDetailController.createUserInfo();

        Test.startTest();
        UserInfo__c userInfo = UserInfoDetailController.getUserInfoDetailByMyId('test_myId');
        Test.stopTest();

        System.assertEquals('test_first', userInfo.firstName__c, 'firstName__c not match');
        System.assertEquals('test_last', userInfo.lastName__c, 'lastName__c not match');
        System.assertEquals('test_address', userInfo.address__c, 'address__c not match');
    }
}