public with sharing class AppTokenAKTController {
    @AuraEnabled(cacheable=true)
    public static AppToken_akt__c findAppTokenByAppType(string appType){
        return [
            SELECT appId_akt__c, token_akt__c 
            FROM AppToken_akt__c 
            WHERE appType_akt__c = :appType 
            WITH SECURITY_ENFORCED 
            LIMIT 1
        ];
    }
}
