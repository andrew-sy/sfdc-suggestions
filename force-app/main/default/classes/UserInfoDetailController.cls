public with sharing class UserInfoDetailController {
    @AuraEnabled(cacheable=true) 
    public static UserInfo__c getUserInfoDetailByMyId(string myId){
        return [
            SELECT firstName__c, lastName__c, address__c
            FROM UserInfo__c
            WHERE myId__c = :myId
            WITH SECURITY_ENFORCED
            LIMIT 1
        ];
    }
}
